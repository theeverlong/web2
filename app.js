const express = require('express');
const app = express();
const path = require('path');
const mustache = require('mustache-express');
const morgan = require('morgan');
const Story = require('./models/story');
const moment = require('moment');

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));

app.use(express.static('public'));

const viewsDir = path.join(__dirname, 'views');
app.engine("mst", mustache(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');

const apiRouter = require('./routes/api');
const developerRouter = require('./routes/developer');
const usersRouter = require('./routes/users');
const storiesRouter = require('./routes/stories');
const highlightsRouter = require('./routes/highlights');
const authRouter = require('./routes/auth');
const reactionRouter = require('./routes/reactions')

const session = require('express-session');
const passport = require('passport');

app.use(morgan('dev'));

app.use(session({
    secret: 'secret',
    resave: false,
    saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());

app.use('/api/v1', apiRouter);
app.use('/developer/v1', developerRouter);
app.use('/users', usersRouter);
app.use('/stories', storiesRouter);
app.use('/highlights', highlightsRouter);
app.use('/auth', authRouter);
app.use('/reactions', reactionRouter);

app.get('/', (req, res) => {
    if(req.user) {
        checkIsStoriesExpired();
        res.render('home', {
            isSignedIn: (typeof req.user === 'undefined') ? 'display: none' : '',
            isSignedOut: (typeof req.user !== 'undefined') ? 'display: none' : '',
            user: req.user 
        });
    }
    else{
        res.render('index', {
            isSignedIn: (typeof req.user === 'undefined') ? 'display: none' : '',
            isSignedOut: (typeof req.user !== 'undefined') ? 'display: none' : '',
            user: req.user
        });
    }
});

app.get('/about', (req, res) => {
    res.render('about', {
        isSignedIn: (typeof req.user === 'undefined') ? 'display: none' : '',
        isSignedOut: (typeof req.user !== 'undefined') ? 'display: none' : '',
        user: req.user
    });
})

function checkAdmin(user) {
    if (user) {
        if (user.role === 1) return '';
        else return 'display: none';
    }
    else return 'display: none';
}

app.get('*', (req, res) => {
    res.render('404', {
        isSignedIn: (typeof req.user === 'undefined') ? 'display: none' : '',
        isSignedOut: (typeof req.user !== 'undefined') ? 'display: none' : '',
        user: req.user
    });
});

app.listen(process.env.PORT || 3000);

async function checkIsStoriesExpired()
{
    const stories = await Story.getAll();
    for(let i = 0; i < stories.length; i++)
    {
        if((moment(new Date()) - moment(stories[i].expiresAt)) > 0)
        {
            console.log(stories[i].title + " no more public");
            stories[i].isPublic = false;
            await Story.update(stories[i]);
        }
    }
}

