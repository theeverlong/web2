const TelegramBot = require('node-telegram-bot-api');
const mybot = new TelegramBot('991142671:AAHYix5EsY9vu1EE1LSmHErjASBVliV2yL4', { polling: true });
const Story = require('./models/story');
const User = require('./models/user');

console.log('bot started');

mybot.onText(/\/start/, (msg) => {
    (async () => {
        let user = await User.getByTelegramUsername(msg.chat.username);
        if (user) {
            user.telegramChatID = msg.chat.id;
            await User.findAndUpdate(user);
            mybot.sendMessage(msg.chat.id, "Connected!");
        }
        else {
        }
    })();
});

mybot.onText(/\/stories/, (msg) => {
    (async () => {
        const user = await User.getByTelegramUsername(msg.chat.username);
        if (user) {
            const stories = await Story.getAllUserPublicStoriesById(user.id);
            if (stories.length === 0) mybot.sendMessage(user.telegramChatID, 'You don\'t have any active stories');
            else {
                const storiesLinks = stories.map(story => {
                    return '<a href=\"http://carpediemgram.herokuapp.com/stories/' + story.id + '\">' + story.title + '</a>';
                });
                const string = storiesLinks.join('\n');
                const send = 'Your active stories: \n' + string;
                mybot.sendMessage(user.telegramChatID, send, { parse_mode: 'HTML' });
            }
        }
    })();

});

mybot.onText(/\/views/, (msg) => {
    (async () => {
        const user = await User.getByTelegramUsername(msg.chat.username);
        if (user) {
            const stories = await Story.getAllUserStoriesById(user.id);
            if (stories.length === 0) mybot.sendMessage(user.telegramChatID, "You don't have any active stories'");
            else {
                let arrayTextCallback = [];
        
                for (let i = 0; i < stories.length; i++) {
                    let textCallback = [];
                    textCallback.push({ text: stories[i].title, callback_data: stories[i].id });
                    arrayTextCallback.push(textCallback);
                }
                let options = {
                    reply_markup: JSON.stringify({
                        inline_keyboard: arrayTextCallback
                    })
                };

                mybot.sendMessage(user.telegramChatID, "Select story:", options);
            }

        }

    })();

});

mybot.on('callback_query', (msg) => {
    (async () => {
        const answer = msg.data;
        const story = await Story.getById(answer);
        
        const user = await User.getByTelegramUsername(msg.from.username);
        let viewers = [];
        if(story.viewsIDs){
            if(story.viewsIDs.length === 0) mybot.sendMessage(user.telegramChatID, story.title + " was seen by nobody");
            for(let i = 0; i < story.viewsIDs.length; i++) {
                const viewer = await User.getById(story.viewsIDs[i]);
                const item = "<a href=\"http://carpediemgram.herokuapp.com/users/" + viewer.id + "\">" + viewer.login + "</a>";
                viewers.push(item);
            }
            const string = viewers.join('\n');
            if(user.id.toString() === story.authorId.toString()){
                mybot.sendMessage(user.telegramChatID, story.title + " was seen by:\n" + string, { parse_mode: 'HTML' });
            }
        }else mybot.sendMessage(user.telegramChatID, story.title + " was seen by nobody");
       
    })();
});

module.exports = mybot;