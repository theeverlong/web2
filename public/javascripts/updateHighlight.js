async function validateHighlight() {
    let isSubmit = true;
    const title = document.getElementById('title').value;
    const desc = document.getElementById('desc').value;

    if(title.length > 50 || title.length < 1)
    {
        document.getElementById('errormsg').innerHTML = (title.length < 1) ? 'Title must be at least 1 character long' : 'Title must be 50 characters long or fewer';
        document.getElementById('errormsgdiv').style.display = '';
        isSubmit = false;
        return;
    }

    if(desc.length > 1000)
    {
        document.getElementById('errormsg').innerHTML = 'Description must be 1000 characters long or fewer';
        document.getElementById('errormsgdiv').style.display = '';
        isSubmit = false;
        return;
    }

    if (isSubmit === true) document.getElementById('newHighlightForm').submit();
}

function closeError() {
    document.getElementById('errormsgdiv').style.display = 'none';
}

function preview_image(event) {
    const reader = new FileReader();

    const fileInput = document.getElementById('highlightImage');
    if (fileInput !== null) {
        const filePath = fileInput.value;
        const allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
        if (!allowedExtensions.exec(filePath)) {
            document.getElementById('errormsg').innerHTML = 'Please upload file having extensions .jpeg/.jpg/.png only.';
            document.getElementById('errormsgdiv').style.display = '';
            fileInput.value = '';


        }else{
            reader.onload = function () {
                const output = document.getElementById('output_image');
                output.src = reader.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    }

   
}
