
const pageSize = 5;
let pagesTotal;
let currentPage;

let searchReq = '';
let usersTemplate;

const prevPageButton = document.getElementById('prevPage');
const nextPageButton = document.getElementById('nextPage');
const pagesTotalElement = document.getElementById('pagesTotal');
const currentPageElement = document.getElementById('currentPage');

const search = document.getElementById('search');
const searchButton = document.getElementById('searchButton');

const pagination = document.getElementById('pagination');
const res = document.getElementById('resMessage');
res.style.display = 'none';

const usersRendered = document.getElementById('usersRendered');
{
    Promise.all([
         fetch("/templates/home.mst").then(x => x.text()),
         fetch("/api/v1/users?page=1").then(x => {
             if (x.ok === false) throw '404';
             return x.json();
         }),
     ])
        .then(([templateStr, itemsData]) => {
            usersTemplate = templateStr;
            currentPage = 1;
            pagesTotal = Math.ceil(itemsData.usersLength / pageSize);
            pagesTotalElement.innerHTML = pagesTotal;
            currentPageElement.innerHTML = 1;
            const dataObject = { users: itemsData.users };
            usersRendered.innerHTML = Mustache.render(usersTemplate, dataObject);
        })
        .catch(error => {
            pagination.style.display = 'none';  
            storiesRendered.innerHTML = "No stories found";
            usersRendered.innerHTML = '';
        });

}

prevPageButton.addEventListener('click', () => {
    if (currentPage > 1) {
        currentPage -= 1;
        currentPageElement.innerHTML = currentPage;
        loadStories();
    }
});

nextPageButton.addEventListener('click', () => {
    if (currentPage < pagesTotal) {
        currentPage += 1;
        currentPageElement.innerHTML = currentPage;
        loadStories();
    }
});

searchButton.addEventListener('click', () => {
    searchReq = search.value;
    if (searchReq !== '') {
        currentPage = 1;
        currentPageElement.innerHTML = currentPage;
    }
    loadStories();
});

search.addEventListener('keyup', () => {
    if (event.keyCode === 13) {
        document.getElementById('searchButton').click();
    }
});

function loadStories() {
{
    fetch("/api/v1/users?page=" + currentPage + "&search=" + searchReq)
            .then(x => {
                if (x.ok === false) throw '404';
                return x.json();
            })
            .then(itemsData => {
                res.style.display = 'none';
                pagination.style.display = '';  
                pagesTotal = Math.ceil(itemsData.usersLength / pageSize);
                pagesTotalElement.innerHTML = pagesTotal;
                currentPageElement.innerHTML = 1;
                const dataObject = { users: itemsData.users };
                usersRendered.innerHTML = Mustache.render(usersTemplate, dataObject);
            })
            .catch(error => {
                pagination.style.display = 'none';  
                res.style.display = '';
                usersRendered.innerHTML = '';
            });
    }
}