const renderedUserId = document.getElementById('renderedUserID').value;

const pageSize = 3;
let storiesPagesTotal;
let storiesCurrentPage;
let highlightsPagesTotal;

let highlightsCurrentPage;

let storiesTemplate;
let highlightsTemplate;

const prevStoryPageButton = document.getElementById('prevPage');
const nextStoryPageButton = document.getElementById('nextPage');
const pagesStoriesTotalElement = document.getElementById('pagesTotal');
const currentStoryPageElement = document.getElementById('currentPage');
const storiesRendered = document.getElementById('storiesRendered');
const storiesPagination = document.getElementById('pagination');

const prevHighlightPageButton = document.getElementById('prevHighlightPage');
const nextHighlightPageButton = document.getElementById('nextHighlightPage');
const pagesHighlightsTotalElement = document.getElementById('pagesHighlightsTotal');
const currentHighlightPageElement = document.getElementById('currentHighlightPage');
const highlightsRendered = document.getElementById('highlightsRendered');
const highlightsPagination = document.getElementById('highlightsPagination');

storiesPagination.style.display = 'none';
highlightsPagination.style.display = 'none';

start();

async function start() {
    const fetchStoriesTemplateResults = await fetch('/templates/stories.mst');
    const fetchHighlightsTemplateResults = await fetch('/templates/highlights.mst');

    storiesTemplate = await fetchStoriesTemplateResults.text();
    highlightsTemplate = await fetchHighlightsTemplateResults.text();

    try {
        const fetchStoriesResults = await fetch('/api/v1/publicUserStories/' + renderedUserId);
        let storiesJSON = await fetchStoriesResults.json();
        const fetchHighlightsResults = await fetch('/api/v1/userHighlights/' + renderedUserId);        
        let highlightsJSON = await fetchHighlightsResults.json();

        storiesCurrentPage = 1;
        highlightsCurrentPage = 1;

        if (storiesJSON.storiesLength !== 0) storiesPagination.style.display = '';

        storiesPagesTotal = Math.ceil(storiesJSON.storiesLength / pageSize);
        pagesStoriesTotalElement.innerHTML = storiesPagesTotal;

        currentStoryPageElement.innerHTML = 1;
        const storiesObject = { stories: storiesJSON.stories };
        storiesRendered.innerHTML = Mustache.render(storiesTemplate, storiesObject);

        if (highlightsJSON.highlightsLength !== 0) highlightsPagination.style.display = '';

        highlightsPagesTotal = Math.ceil(highlightsJSON.highlightsLength / pageSize);
        pagesHighlightsTotalElement.innerHTML = highlightsPagesTotal;

        currentHighlightPageElement.innerHTML = 1;
        const highlightsObject = { highlights: highlightsJSON.highlights };
        highlightsRendered.innerHTML = Mustache.render(highlightsTemplate, highlightsObject);

    } catch (error) {
        console.log(error);
    }
};

prevStoryPageButton.addEventListener('click', () => {
    if (storiesCurrentPage > 1) {
        storiesCurrentPage -= 1;
        currentStoryPageElement.innerHTML = storiesCurrentPage;
        loadStories();
    }
});

nextStoryPageButton.addEventListener('click', () => {
    if (storiesCurrentPage < storiesPagesTotal) {
        storiesCurrentPage += 1;
        currentStoryPageElement.innerHTML = storiesCurrentPage;
        loadStories();
    }
});

prevHighlightPageButton.addEventListener('click', () => {
    if (highlightsCurrentPage > 1) {
        highlightsCurrentPage -= 1;
        currentHighlightPageElement.innerHTML = highlightsCurrentPage;
        loadHighlights();
    }
});

nextHighlightPageButton.addEventListener('click', () => {
    if (highlightsCurrentPage < highlightsPagesTotal) {
        highlightsCurrentPage += 1;
        currentHighlightPageElement.innerHTML = highlightsCurrentPage;
        loadHighlights();
    }
});

async function loadStories() {
    try {
        const fetchStoriesResults = await fetch('/api/v1/userStories/' + renderedUserId + "?page=" + storiesCurrentPage);
        const storiesJSON = await fetchStoriesResults.json();
        console.log(storiesJSON);
        const storiesObject = { stories: storiesJSON.stories };
        storiesRendered.innerHTML = Mustache.render(storiesTemplate, storiesObject);
    } catch (error) {
    }
};

async function loadHighlights() {

    try {
        const fetchStoriesResults = await fetch('/api/v1/userHighlights/' + renderedUserId + "?page=" + highlightsCurrentPage);
        const highlightsJSON = await fetchStoriesResults.json();
        const highlightsObject = { highlights: highlightsJSON.highlights };
        highlightsRendered.innerHTML = Mustache.render(highlightsTemplate, highlightsObject);
    } catch (error) {
    }
};