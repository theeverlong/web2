const pageSize = 5;
let pagesTotal;
let currentPage;

const prevPageButton = document.getElementById('prevPage');
const nextPageButton = document.getElementById('nextPage');
const pagesTotalElement = document.getElementById('pagesTotal');
const currentPageElement = document.getElementById('currentPage');
const pagination = document.getElementById('pagination');
const res = document.getElementById('resMessage');

const prevPageButton1 = document.getElementById('prevPage1');
const nextPageButton1 = document.getElementById('nextPage1');
const pagesTotalElement1 = document.getElementById('pagesTotal1');
const currentPageElement1 = document.getElementById('currentPage1');


const pagination1 = document.getElementById('pagination1');
const res1 = document.getElementById('resMessage');

const rendered = document.getElementById('receivedRendered');
const rendered1 = document.getElementById('sentRendered');

pagination.style.display = 'none';
pagination1.style.display = 'none';

start();

async function start() {

    const res1 = await fetch("/templates/messages.mst");
    const res2 = await fetch("/api/v1/receivedReactions");
    const res3 = await fetch("/api/v1/sentReactions");

    let templateStr = await res1.text();
    let itemsData = await res2.json();
    let itemsData1 = await res3.json();

    messagesTemplate = templateStr;
    currentPage = 1;
    currentPage1 = 1;
    pagesTotal = Math.ceil(itemsData.receivedReactionsLength / pageSize);
    pagesTotal1 = Math.ceil(itemsData1.sentReactionsLength / pageSize);

    if (itemsData.receivedReactionsLength > 0) pagination.style.display = '';
    if (itemsData1.sentReactionsLength > 0) pagination1.style.display = '';

    pagesTotalElement.innerHTML = pagesTotal;
    pagesTotalElement1.innerHTML = pagesTotal1;

    currentPageElement.innerHTML = 1;
    currentPageElement1.innerHTML = 1;

    let recMessages = itemsData.receivedReactions;
    let sentMessages = itemsData1.sentReactions;

    for (let i = 0; i < recMessages.length; i++) {
        const result = await fetch('/api/v1/users/' + recMessages[i].authorID);
        const json = await result.json();
        recMessages[i].author = json.user;
        recMessages[i].timePassed = moment(recMessages[i].time).fromNow();
    }
    for (let i = 0; i < sentMessages.length; i++) {
        const result = await fetch('/api/v1/users/' + sentMessages[i].receiverID);
        const jsonRes = await result.json();
        sentMessages[i].author = jsonRes.user;
        sentMessages[i].timePassed = moment(sentMessages[i].time).fromNow();
    }
    for (let i = 0; i < recMessages.length; i++) {
        const result = await fetch('/api/v1/stories/' + recMessages[i].storyID);
        if (result.ok) {
            const jsonRes = await result.json();
            recMessages[i].story = jsonRes.story;
        }

    }
    for (let i = 0; i < sentMessages.length; i++) {
        const result = await fetch('/api/v1/stories/' + sentMessages[i].storyID);
        if (result.ok) {
            const jsonRes = await result.json();
            sentMessages[i].story = jsonRes.story;
        }

    }

    let dataObject = {
        messages: recMessages,
        isDeleteVisible: ''
    };
    let dataObject1 = {
        messages: sentMessages,
        isDeleteVisible: 'display: none'
    };


    rendered.innerHTML = Mustache.render(messagesTemplate, dataObject);
    rendered1.innerHTML = Mustache.render(messagesTemplate, dataObject1);


}

prevPageButton.addEventListener('click', () => {
    if (currentPage > 1) {
        currentPage -= 1;
        currentPageElement.innerHTML = currentPage;
        loadReceivedMessages();
    }
});

nextPageButton.addEventListener('click', () => {
    if (currentPage < pagesTotal) {
        currentPage += 1;
        currentPageElement.innerHTML = currentPage;
        loadReceivedMessages();
    }
});

prevPageButton1.addEventListener('click', () => {
    if (currentPage1 > 1) {
        currentPage1 -= 1;
        currentPageElement1.innerHTML = currentPage1;
        loadSentMessages();
    }
});

nextPageButton1.addEventListener('click', () => {
    if (currentPage1 < pagesTotal1) {
        currentPage1 += 1;
        currentPageElement1.innerHTML = currentPage1;
        loadSentMessages();
    }
});

async function loadReceivedMessages() {

    const res2 = await fetch("/api/v1/receivedReactions?page=" + currentPage);

    let itemsData = await res2.json();

    pagesTotal = Math.ceil(itemsData.receivedReactionsLength / pageSize);

    pagesTotalElement.innerHTML = pagesTotal;


    let recMessages = itemsData.receivedReactions;

    for (let i = 0; i < recMessages.length; i++) {
        const result = await fetch('/api/v1/users/' + recMessages[i].authorID);
        const json = await result.json();
        recMessages[i].author = json.user;
        recMessages[i].timePassed = moment(recMessages[i].time).fromNow();
    }

    for (let i = 0; i < recMessages.length; i++) {
        const result = await fetch('/api/v1/stories/' + recMessages[i].storyID);
        const jsonRes = await result.json();
        recMessages[i].story = jsonRes.story;
    }

    let dataObject = {
        messages: recMessages,
        isDeleteVisible: ''
    };
    rendered.innerHTML = Mustache.render(messagesTemplate, dataObject);

}

async function loadSentMessages() {

    const res3 = await fetch("/api/v1/sentReactions?page=" + currentPage1);

    let itemsData1 = await res3.json();


    if (itemsData1.sentReactionsLength > 0) pagination1.style.display = '';


    let sentMessages = itemsData1.sentReactions;


    for (let i = 0; i < sentMessages.length; i++) {
        const result = await fetch('/api/v1/users/' + sentMessages[i].receiverID);
        const jsonRes = await result.json();
        sentMessages[i].author = jsonRes.user;
        sentMessages[i].timePassed = moment(sentMessages[i].time).fromNow();
    }

    for (let i = 0; i < sentMessages.length; i++) {
        const result = await fetch('/api/v1/stories/' + sentMessages[i].storyID);
        const jsonRes = await result.json();
        sentMessages[i].story = jsonRes.story;
    }


    let dataObject1 = {
        messages: sentMessages,
        isDeleteVisible: 'display: none'
    };

    rendered1.innerHTML = Mustache.render(messagesTemplate, dataObject1);


}


