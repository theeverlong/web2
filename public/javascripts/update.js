async function validateUserUpdate() {
    let isSubmit = true;


    const usernameElem = document.getElementById('username');
    const fullNameElem = document.getElementById('fullname');
    const bioElem = document.getElementById('bio');

    const x = await fetch('/api/v1/validUsername/' + usernameElem.value);

    if (!x.ok) {

        const data = await x.json();
        if (data.username.toString() !== usernameElem.value.toString()) {
            document.getElementById('errormsg').innerHTML = 'Username already exists';
            document.getElementById('errormsgdiv').style.display = '';
            isSubmit = false;
            return;
        }

    }
    if (usernameElem.value.length > 20 || usernameElem.value.length < 3) {
        document.getElementById('errormsg').innerHTML = (usernameElem.value.length < 3) ? 'Your username must be at least 3 characters long' : 'Your username must be 20 characters long or fewer';
        document.getElementById('errormsgdiv').style.display = '';
        isSubmit = false;
        return;

    }
    if (fullNameElem.value.length > 100) {
        document.getElementById('errormsg').innerHTML = 'Your fullname must be 100 characters long or fewer';
        document.getElementById('errormsgdiv').style.display = '';
        isSubmit = false;
        return;

    }
    if (bioElem.value.length > 1000) {
        document.getElementById('errormsg').innerHTML = 'Your 1000 must be 1000 characters long or fewer';
        document.getElementById('errormsgdiv').style.display = '';
        isSubmit = false;
        return;

    }
    const username = document.getElementById('username').value;

    if (isSubmit === true) document.getElementById('form11').submit();
}

function closeError() {
    document.getElementById('errormsgdiv').style.display = 'none';
}

function preview_image(event) {
    const reader = new FileReader();

    const fileInput = document.getElementById('profilePhoto');
    if (fileInput !== null) {
        console.log('photo', 'not null!!!!!!!!!');
        const filePath = fileInput.value;
        const allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
        if (!allowedExtensions.exec(filePath)) {
            document.getElementById('errormsg').innerHTML = 'Please upload file having extensions .jpeg/.jpg/.png only.';
            document.getElementById('errormsgdiv').style.display = '';
            fileInput.value = '';


        }else{
            reader.onload = function () {
                const output = document.getElementById('output_image');
                output.src = reader.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    }

   
}
