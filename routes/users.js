const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const User = require('../models/user');
const asyncHandler = require('express-async-handler');
const Datauri = require('datauri');
require('dotenv').config();

const multer = require('multer');
const path = require('path');

const storage = multer.memoryStorage();

let upload = multer({
    storage: storage,
    limits: { fileSize: 1000000 * 5 },
    fileFilter: function (req, file, cb) {
        if (file.mimetype !== 'image/png' && file.mimetype !== 'image/jpeg') {
            req.fileValidationError = true;
            return cb(new Error('Incorrect format'));
        }
        return cb(null, true);
    }
});
const dUri = new Datauri();
const dataUri = req => dUri.format(path.extname(req.file.originalname).toString(), req.file.buffer);
const cloudinary = require('cloudinary');


cloudinary.config({
    cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET
});


router.get('/:id', (req, res) => {
    if (!req.user) return res.redirect('/auth/login');
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) res.redirect('/404');
    User.getById(req.params.id)
        .then(user => {
            if (!user) return res.redirect('/404');
            else {
                let isVisibleTip = '';
                if(req.user.id.toString() !== req.params.id.toString() || user.telegramChatID) isVisibleTip = 'display: none';
                res.render('user', {
                    isSignedIn: (typeof req.user === 'undefined') ? 'display: none' : '',
                    isSignedOut: (typeof req.user !== 'undefined') ? 'display: none' : '',
                    renderedUser: user,
                    isVisibleEdit: (req.user.id.toString() !== req.params.id.toString()) ? 'display: none' : '',
                    isVisibleTip: isVisibleTip,
                    isChangeRoleVisible: (req.user.role === 1) ? '' : 'display: none',
                    isCheckedAdmin: (user.role === 1) ? 'checked' : '',
                    isCheckedUser: (user.role === 0) ? 'checked' : '',
                    user: req.user
                });
            }
        })
        .catch(err => res.status(500).send(err.toString()));
});

router.get('/update/:id', (req, res) => {
    if (!req.user) res.redirect('/auth/login');
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) res.redirect('/404');
    if (req.user.id.toString() !== req.params.id.toString()) res.status(403).send('Forbidden');
    User.getById(req.params.id)
        .then(user => {
            if (!user) return res.redirect('/404');
            else res.render('update', {
                isSignedIn: (typeof req.user === 'undefined') ? 'display: none' : '',
                isSignedOut: (typeof req.user !== 'undefined') ? 'display: none' : '',
                renderedUser: user,
                user: req.user
            })
        })
        .catch(err => res.status(500).send(err.toString()));
});

router.post('/update/:id', (req, res) => {
    console.log('in update');
    if (!req.user) return res.redirect('/auth/login');
    if (req.user.id.toString() !== req.params.id.toString()) res.status(403).send('Forbidden');
    upload.single('profilePhoto')(req, res, err => {
        if (err) {
            res.status(400).send({ error: 'Bad Request' });
        }
        else {
            if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.redirect('/404');
            let userToUpdate;
            User.getById(req.params.id)
                .then(user => {
                    if (user) {
                        if (typeof req.body.username !== 'undefined') user.login = req.body.username;
                        if (user.login.length > 20 || user.login.length < 3) throw '400';
                        if (typeof req.body.fullname !== 'undefined') user.fullname = req.body.fullname;
                        if (user.fullname.length > 100) throw '400';
                        if (typeof req.body.bio !== 'undefined') user.bio = req.body.bio;
                        if (user.bio.length > 1000) throw '400';
                        if (typeof req.body.telegramUsername !== 'undefined') user.telegramUsername = req.body.telegramUsername;
                        if (user.telegramUsername.length > 100) throw '400';
                        userToUpdate = user;

                        if (req.file) {
                            const file = dataUri(req).content;
                            return cloudinary.uploader.upload(file);
                        }
                        else {
                            return Promise.resolve(null);
                        }
                    }
                    else throw '400';
                })
                .then(upload => {
                    if (upload) {
                        userToUpdate.avaUrl = upload.url;
                    }
                    return User.findAndUpdate(userToUpdate);
                })
                .then(updatedUser => {
                    console.log('updated: \n' + updatedUser);
                    res.redirect('/users/' + updatedUser.id);
                })
                .catch(error => {
                    if (error === '400') res.status(400).send({ error: 'Bad Request' });
                    if (error.http_code == 400) res.status(400).send({ error: 'Bad Request' });
                    res.status(500).send({ error: 'Internal Server Error' });
                });
        }
    });
});


router.post('/updateRole/:id', asyncHandler(async (req, res) => {

    if (!req.user) return res.redirect('/auth/login');
    if(req.user.role !== 1) return res.status(403).send({message: 'Forbidden'});

    try {
        const user = await User.getById(req.params.id);
        if(user)
        {
            user.role = req.body.radios;
            await User.update(user);
            return res.redirect('/users/'+user.id);
        }

    }
    catch(error){
        console.log(error);
        res.status(500).send({ error: 'Internal Server Error' });
    }
}));


module.exports = router;