const express = require('express');
const router = express.Router();
const User = require('../models/user');
const Highlight = require('../models/highlight');
const Story = require('../models/story');
const Datauri = require('datauri');
require('dotenv').config();
const asyncHandler = require('express-async-handler')
const mongoose = require('mongoose');

const multer = require('multer');
const path = require('path');

const storage = multer.memoryStorage();

let upload = multer({
    storage: storage,
    limits: { fileSize: 1000000 * 5 },
    fileFilter: function (req, file, cb) {
        if (file.mimetype !== 'image/png' && file.mimetype !== 'image/jpeg') {
            req.fileValidationError = true;
            return cb(new Error('Incorrect format'));
        }
        return cb(null, true);
    }
});

const dUri = new Datauri();
const dataUri = req => dUri.format(path.extname(req.file.originalname).toString(), req.file.buffer);
const cloudinary = require('cloudinary');


cloudinary.config({
    cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET
});

router.get('/new', asyncHandler(async (req, res) => {
    if (!req.user) return res.redirect('/auth/login');
    try {
        const stories = await Story.getAllUserStoriesById(req.user.id);
        res.render('newHighlight', {
            stories: stories,
            isSignedIn: (typeof req.user === 'undefined') ? 'display: none' : '',
            isSignedOut: (typeof req.user !== 'undefined') ? 'display: none' : '',
            user: req.user
        })
    }
    catch (error) {
        console.log(err);
        return res.status(500).send({ error: 'Internal Server Error' })
    }
}));


router.get('/:id', asyncHandler(async (req, res) => {
    if (!req.user) return res.redirect('/auth/login');
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.redirect('/404');
    try {
        const highlight = await Highlight.getById(req.params.id);
        const stories = await Story.getAllByIDs(highlight.storiesIDs);
        const renderedUser = await User.getById(highlight.authorID);
        res.render('highlight', {
            renderedUser: renderedUser,
            highlight: highlight,
            stories: stories,
            isSignedIn: (typeof req.user === 'undefined') ? 'display: none' : '',
            isSignedOut: (typeof req.user !== 'undefined') ? 'display: none' : '',
            isEditing: (req.user.id.toString() === renderedUser.id.toString()) ? '' : 'display: none',
            user: req.user
        })
    }
    catch (error) {
        console.log(error);
        return res.status(500).send({ error: 'Internal Server Error' });
    }
}));

router.post('/highlight', upload.single('highlightImage'), asyncHandler(async (req, res) => {
    if (!req.user) return res.redirect('/auth/login');
    try {
        if (req.file) {
            if (req.fileValidationError) {
                return res.status(400).send({ error: 'Invalid image file' });
            }
            const image = dataUri(req).content;
            const titleImageUrl = await uploadToCloudinary(image);
            if (typeof req.body.title === 'undefined') return res.status(400).send({ error: 'Bad Request' });
            const highlightTitle = req.body.title;
            if (highlightTitle.length < 1 || highlightTitle.length > 50) return res.status(400).send({ error: 'Bad Request' });
            const highlightDesc = (typeof req.body.desc === 'undefined') ? '' : req.body.desc;
            if (highlightDesc.length > 1000) return res.status(400).send({ error: 'Bad Request' });
            const storiesIds = (req.body.stories === undefined) ? [] : req.body.stories;
            const newHighlight = await Highlight.insert(new Highlight(null, highlightTitle, titleImageUrl, highlightDesc, storiesIds, req.user.id));
            return res.redirect('/highlights/' + newHighlight.id);
        }
        else {
            console.log('no req.file');
            return res.status(400).send({ error: 'Bad Request' });
        }
    }
    catch (error) {
        console.log(error);
        return res.status(500).send({ error: 'Internal Server Error' });
    }

}));

router.get('/update/:id', asyncHandler(async (req, res) => {
    if(!req.user) return res.redirect('/auth/login');
    try{
        const stories = await Story.getAllUserStoriesById(req.user.id);
        const highlight = await Highlight.getById(req.params.id);
        if(highlight.authorID.toString() !== req.user.id.toString()) return res.status(403).json({ message: 'Forbidden'});
        for(let i = 0; i < stories.length; i++) {
            stories[i].checked = '';
            for(let id of highlight.storiesIDs)
            {
                if(stories[i]._id.toString() === id.toString()){
                    stories[i].checked = 'checked';
                }
            }
        }
        res.render('updateHighlight', { 
            stories: stories,
            highlight: highlight,
            isSignedIn: (typeof req.user === 'undefined') ? 'display: none' : '',
            isSignedOut: (typeof req.user !== 'undefined') ? 'display: none' : '',
            user: req.user
        })
    }
    catch (error) {
        console.log(error);
        return res.status(500).send({ error: 'Internal Server Error' });
    }
  
}));


router.post('/delete/:id', asyncHandler(async (req, res) => {
    if (!req.user) return res.redirect('/auth/login');
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.redirect('/404');
    try {
        const highlight = await Highlight.getById(req.params.id);
        if (req.user.id.toString() !== highlight.authorID.toString()) return res.status(404).send({ 'error': 'Forbidden' });
        image = highlight.titleImageUrl;
        await Highlight.deleteById(req.params.id);
        await deleteImageCloudinary(image);
        return res.redirect('/users/' + req.user.id);
    }
    catch (error) {
        console.log(error);
        res.status(500).send({ error: 'Internal Server Error' });
    }

}))

router.post('/update/:id', upload.none(), (req, res) => {
    Highlight.getById(req.params.id)
        .then((highlight) => {
            highlight.storiesIDs = (typeof req.body.stories === 'undefined') ? [] : req.body.stories;
            highlight.description = (typeof req.body.desc === 'undefined') ? '' : req.body.desc;
            highlight.title = (typeof req.body.desc === 'undefined') ? 'default' : req.body.title;
            return Highlight.update(highlight);
        })
        .then(() => res.redirect('/highlights/' + req.params.id))
        .catch(err => res.status(500).send(err.toString()));
});

function uploadToCloudinary(image) {
    return new Promise((resolve, reject) => {
        cloudinary.uploader.upload(image,
            (result, error) => {
                if (error) return reject(error);
                return resolve(result.url);
            })
    });
}

function deleteImageCloudinary(image) {
    return new Promise((resolve, reject) => {
        cloudinary.v2.api.delete_resources(image,
            (error, result) => {
                if (error) reject(error);
                else resolve(result);
            });
    });
}

module.exports = router;
