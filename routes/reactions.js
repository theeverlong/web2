const express = require('express');
const router = express.Router();
const Story = require('../models/story');

const User = require('../models/user');
const asyncHandler = require('express-async-handler')
const mongoose = require('mongoose');
const Reaction = require('../models/reaction');

const multer = require('multer');

const mybot = require('../bot');

const storage = multer.memoryStorage();

let upload = multer({
    storage: storage,
    limits: { fileSize: 1000000 * 5 },
    fileFilter: function (req, file, cb) {
        if (file.mimetype !== 'image/png' && file.mimetype !== 'image/jpeg') {
            req.fileValidationError = true;
            return cb(new Error('Incorrect format'));
        }
        return cb(null, true);
    }
});

router.get('/', asyncHandler(async (req, res) => {
    if (!req.user) res.redirect('/auth/login');
    try {
        res.render('reactions', {
            user: req.user,
            isSignedIn: (typeof req.user === 'undefined') ? 'display: none' : '',
            isSignedOut: (typeof req.user !== 'undefined') ? 'display: none' : ''
        })
    }
    catch (error) {
        console.log(error);
        return res.status(500).send({ error: 'Internal Server Error' });
    }
}));

router.post('/new', upload.none(), asyncHandler(async (req, res) => {
    if (!req.user) return res.redirect('/auth/login');
    try {
        if (req.user.id === req.body.receiverID) return res.status(400).send({ error: 'Bad Request' });
        if (typeof req.body.reactionMessage === 'undefined') return res.status('400').send({ error: 'Bad Request' });
        const message = req.body.reactionMessage;
        if (message.length > 140) return res.status('400').send({ error: 'Bad Request' });
        const reaction = await Reaction.insert(new Reaction(null, message, Date.now(), req.user.id, req.body.receiverID, req.body.storyID));

        const sender = await User.getById(req.user.id);
        const receiver = await User.getById(req.body.receiverID);
        const story = await Story.getById(req.body.storyID);
        if (receiver.telegramChatID) {
            const send = '<a href=\"http://carpediemgram.herokuapp.com/users/' + sender.id + '\">' + sender.login
                + '</a>' + ' has reacted to your story ' + '<a href=\"http://carpediemgram.herokuapp.com/stories/'
                + story.id + '\">' + story.title + '</a>:\n' + '\n<em>' + message + '</em>';
            mybot.sendMessage(receiver.telegramChatID, send, { parse_mode: 'HTML' });

        }
        res.redirect('/reactions');
    }
    catch (error) {
        console.log(error);
        return res.status(500).send({ error: 'Internal Server Error' });
    }
}));

router.post('/delete/:id', upload.none(), asyncHandler(async (req, res) => {
    if (!req.user) return res.redirect('/auth/login');
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.redirect('/404');
    try {
        const reaction = await Reaction.getById(req.params.id);
        if (req.user.id.toString() !== reaction.receiverID.toString()) return res.status(403).send({ error: 'Forbidden' });
        else {
            await Reaction.deleteById(req.params.id);
            return res.redirect('/reactions');
        }
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal Server Error' });
    }
}));

module.exports = router;
