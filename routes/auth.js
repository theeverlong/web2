const express = require('express');
const router = express.Router();
const User = require('../models/user');

const saltedMd5 = require('salted-md5');
const serverSalt = 'SUPER-S@LT!';

require('dotenv').config();

const cookieParser = require('cookie-parser');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const BasicStrategy = require('passport-http').BasicStrategy;
const FacebookStrategy = require('passport-facebook').Strategy;

router.use(cookieParser());

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser((id, doneCB) => {
    User.getById(id)
        .then(user => {
            if (!user) doneCB('No user');
            else doneCB(null, user);
        })
        .catch(err => doneCB(err));
});

passport.use(new BasicStrategy(
    function (username, password, done) {
        User.getByLoginAndPasswordHash(username, saltedMd5(password, serverSalt))
            .then(user => {
                if (!user) return done(null, false);
                else return done(null, user);
            })
            .catch(err => done(err));
    }
));

passport.use(new LocalStrategy(
    function (username, password, done) {
        User.getByLoginAndPasswordHash(username, saltedMd5(password, serverSalt))
            .then(user => {
                if (!user) return done(null, false);
                else return done(null, user);
            })
            .catch(err => done(err));
    }
));

passport.use(
    new FacebookStrategy(
        {
            clientID: process.env.FACEBOOK_CLIENT_ID,
            clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
            callbackURL: process.env.FACEBOOK_CALLBACK_URL,
        },
        async function (accessToken, refreshToken, profile, done) {
            try {
                
                let user = await User.getByFacebookID(profile.id); 
                
                if (!user) {
                     const image = 'https://res.cloudinary.com/dw5e6ptdy/image/upload/v1575816573/trzcacak.rs-user-png-650485_jchcnp.png';
                     user = await User.insert(new User(null, 'fb'+ profile.id.toString(), null, 0, '', Date.now(), '', image, null, null, profile.id));
                 }
                return done(null, user);
            } catch (error) {
                return done(error);
            }
        }

    )
);

router.get('/register', (req, res) => {
    if (req.user) res.redirect('/');
    res.render('register', {
        isSignedIn: (typeof req.user === 'undefined') ? 'display: none' : '',
        isSignedOut: (typeof req.user !== 'undefined') ? 'display: none' : ''
    });
});

router.get('/login', (req, res) => {
    if (req.user) res.redirect('/');
    const isErrorRendering = (typeof req.query.error !== 'undefined') ? true : false;
    if (isErrorRendering) {
        res.render('login', {
            errorMessage: req.query.error,
            isVisible: "",
            isVisibleProfileLogout: 'display: none',
            isVisibleUsers: 'display: none',
            isSignedIn: (typeof req.user === 'undefined') ? 'display: none' : '',
            isSignedOut: (typeof req.user !== 'undefined') ? 'display: none' : ''
        });
    }
    else {
        res.render('login', {
            isVisibleProfileLogout: 'display: none',
            isVisible: "display: none",
            isVisibleUsers: 'display: none',
            isSignedIn: (typeof req.user === 'undefined') ? 'display: none' : '',
            isSignedOut: (typeof req.user !== 'undefined') ? 'display: none' : ''
        });
    }
});

router.post('/register', (req, res) => {
    if (req.user) res.redirect('/');
    const login = req.body.username;
    User.checkLoginUniqueness(login)
        .then((user) => {
            if (user) {
                res.redirect('/auth/register');
            }
            else {
                if (login.length < 3 || login.length > 20) throw new Error('');
                let pass;
                if (req.body.password !== req.body.password2) throw new Error('');
                pass = req.body.password;
                if (pass.length < 6 || pass.length > 256) throw new Error('');
                const passHash = saltedMd5(pass, serverSalt);
                const image = 'https://res.cloudinary.com/dw5e6ptdy/image/upload/v1575816573/trzcacak.rs-user-png-650485_jchcnp.png';
                return User.insert(new User(null, login, passHash, 0, '', Date.now(), '', image, null, null, null));
            };
        })
        .then(() => {
            res.redirect('/auth/login');
        })
        .catch((err1) => {
            res.status(500).send(err1.toString());
        });

});

router.post('/login',
    passport.authenticate('local', {
        successRedirect: '/',
        failureRedirect: '/auth/login?error=Incorrect+username+or+password'
    })
);

router.post('/loginFacebook', passport.authenticate('facebook'));

router.get('/loginFacebookCallback', passport.authenticate('facebook', {
    successRedirect: '/',
    failureRedirect: '/auth/login?error=Facebook+error'
}));

router.post('/logout', (req, res) => {
    req.logout();
    res.redirect('/');
});

module.exports = router;