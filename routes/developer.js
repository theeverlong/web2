const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    res.render('api', {
        isSignedIn: (typeof req.user === 'undefined') ? 'display: none' : '',
        isSignedOut: (typeof req.user !== 'undefined') ? 'display: none' : '',
        user: req.user
    });
});

module.exports = router;