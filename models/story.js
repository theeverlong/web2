const mongoose = require('mongoose');
require('dotenv').config();

const dbUrl = process.env.MONGO_URL;

const connectOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true
};

let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

mongoose.connect(dbUrl, connectOptions)
    .then(() => console.log('Mongo database connected'))
    .catch(() => console.log('ERROR: Mongo database not connected'));

const StorySchema = new Schema({
    title: { type: String, default: 'default' },
    publishedAt: { type: Date, default: Date.now },
    expiresAt: { type: Date },
    description: { type: String },
    isPublic: { type: Boolean },
    imageUrl: { type: String },
    authorId: {type: ObjectId, ref: 'User'},
    viewsIDs: [{ type: ObjectId, ref: 'User' }],
    versionKey: false
});

StorySchema.virtual('id').get(function(){
    return this._id;
});

const StoryModel = mongoose.model('Story', StorySchema);

class Story {
    constructor(id, title, publishedAt, expiresAt, description, isPublic, imageUrl, authorId, viewsIDs) {
        this.id = id;
        this.title = title;
        this.publishedAt = publishedAt;
        this.expiresAt = expiresAt;
        this.description = description;
        this.isPublic = isPublic;
        this.imageUrl = imageUrl;
        this.authorId = authorId;
        this.viewsIDs = viewsIDs;
    }

    static getById(id) {
        return StoryModel.findById(id);
    }

    static getAllByIDs(ids){
        return StoryModel.find({_id : {$in : ids}});
    }

    static getAll() {
        return StoryModel.find();
    }

    static insert(story) {
        return new StoryModel(story).save();
    }
    
    static update(story) {
        return StoryModel.update({_id : story.id}, story); 
    }

    static findAndUpdate(story){
        return StoryModel.findOneAndUpdate({ _id: story.id }, story,  {new: true, useFindAndModify: false});
    }   

    static findAndDeleteById(id){
        return StoryModel.findOneAndDelete({ _id: id });
    }

    static deleteById(id) {
        return StoryModel.deleteOne({_id : id});
    }

    static getAllUserStoriesById(id){
        return StoryModel.find({authorId : id});
    }

    static getAllUserPublicStoriesById(id){
        return StoryModel.find({authorId : id, isPublic: true});
    }


}

module.exports = Story;