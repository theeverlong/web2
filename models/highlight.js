const mongoose = require('mongoose');

const dbUrl = process.env.MONGO_URL;

const connectOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true
};

let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

mongoose.connect(dbUrl, connectOptions)
    .then(() => console.log('Mongo database connected'))
    .catch(() => console.log('ERROR: Mongo database not connected'));

const HighlightSchema = new Schema({
    title: { type: String, required: true },
    titleImageUrl: { type: String },
    description: { type: String },
    storiesIDs: [{ type: ObjectId, ref: 'Story' }],
    authorID: {type: ObjectId, ref: 'User'},
    versionKey: false
});

HighlightSchema.virtual('id').get(function () {
    return this._id;
});

const HighlightModel = mongoose.model('Highlight', HighlightSchema);

class Highlight {
    constructor(id, title, titleImageUrl, description, storiesIDs, authorID) {
        this.id = id;
        this.title = title;
        this.titleImageUrl = titleImageUrl;
        this.description = description;
        this.storiesIDs = storiesIDs;
        this.authorID = authorID;
    }

    static getById(id) {
        return HighlightModel.findById(id);
    }

    static getAll() {
        return HighlightModel.find();
    }

    static getAllWithStoryId(id){
        return HighlightModel.find({storiesIDs : id})
    }

    static removeStoryFromAll(id){
        return HighlightModel.update({}, 
            {$pull: {storiesIDs: id}}, 
            {multi: true})
    }

    static insert(highlight) {
        return new HighlightModel(highlight).save();
    }

    static update(highlight) {
        return HighlightModel.updateOne({ _id: highlight.id }, highlight);
    }

    static findAndUpdate(highlight){
        return HighlightModel.findOneAndUpdate({ _id: highlight.id }, highlight,  {new: true, useFindAndModify: false});
    }   

    static deleteById(id) {
        return HighlightModel.deleteOne({ _id: id });
    }

    static findAndDeleteById(id){
        return HighlightModel.findOneAndDelete({ _id: id });
    }

    static getAllUserHighlightsById(id){
        return HighlightModel.find({authorID : id});
    }

}

module.exports = Highlight;
