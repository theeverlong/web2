async function validate(form) {
    let isSubmit = true;
    const username = document.getElementById('username').value;
    const passElement1 = document.getElementById('inputPassword').value;
    const passElement2 = document.getElementById('inputPassword2').value;
    console.log(username, passElement1, passElement2);
    if (username.length < 3 || username.length > 20) {
        document.getElementById('errormsg').innerHTML = (username.length < 3) ? 'Your username must be at least 3 characters long' : 'Your username must be 20 characters long or fewer';
        document.getElementById('errormsgdiv').style.display = '';
        return false;
    }
    else {
        const x =  await fetch('/api/v1/validUsername/'+ username);
                if (!x.ok) {
                    document.getElementById('errormsg').innerHTML = 'Username already exists';
                    document.getElementById('errormsgdiv').style.display = '';
                    isSubmit = false;
                }
                else {
                    if (passElement1 !== passElement2) {
                        document.getElementById('errormsg').innerHTML = 'Passwords don\'t match'
                        document.getElementById('errormsgdiv').style.display = '';
                        isSubmit = false;
                    }

                    if (passElement2.length < 6 || passElement2.length > 256) {
                        document.getElementById('errormsg').innerHTML = (password.length < 3) ? 'Your password must be at least 6 characters long' : 'Your username must be 256 characters long or fewer';
                        document.getElementById('errormsgdiv').style.display = '';
                        isSubmit = false;
                    }
                }
               
    }
    if(isSubmit === true) document.getElementById('form12').submit();
}

function closeError() {
    document.getElementById('errormsgdiv').style.display = 'none';
}

