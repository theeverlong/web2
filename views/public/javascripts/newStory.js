async function validateStory() {
    let isSubmit = true;
    const title = document.getElementById('title').value;
    const desc = document.getElementById('desc').value;
    const expireTime = document.getElementById('time').value;

    if(title.length > 50 || title.length < 1)
    {
        document.getElementById('errormsg').innerHTML = (title.length < 1) ? 'Title must be at least 1 character long' : 'Title must be 50 characters long or fewer';
        document.getElementById('errormsgdiv').style.display = '';
        isSubmit = false;
        return;
    }

    if(desc.length > 1000)
    {
        document.getElementById('errormsg').innerHTML = 'Description must be 1000 characters long or fewer';
        document.getElementById('errormsgdiv').style.display = '';
        isSubmit = false;
        return;
    }
    if(isNaN(expireTime))
    {
        document.getElementById('errormsg').innerHTML = (title.length < 0) ? 'Time must be at least 1 hour' : 'Time must be 24 hours or fewer';
        document.getElementById('errormsgdiv').style.display = '';
        isSubmit = false;
        return;
    }
    if(expireTime > 72 || expireTime < 1)
    {
        document.getElementById('errormsg').innerHTML = (title.length < 0) ? 'Time must be at least 1 hour' : 'Time must be 24 hours or fewer';
        document.getElementById('errormsgdiv').style.display = '';
        isSubmit = false;
        return;
    }

    const fileInput = document.getElementById('storyImage');
    if (fileInput.value === '') {
        document.getElementById('errormsg').innerHTML = 'Upload photo before adding new story.';
        document.getElementById('errormsgdiv').style.display = '';
        isSubmit = false;
        return;
    }

    if (isSubmit === true) document.getElementById('newStoryForm').submit();
}

function closeError() {
    document.getElementById('errormsgdiv').style.display = 'none';
}

function preview_image(event) {
    const reader = new FileReader();

    const fileInput = document.getElementById('storyImage');
    if (fileInput !== null) {
        const filePath = fileInput.value;
        const allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
        if (!allowedExtensions.exec(filePath)) {
            document.getElementById('errormsg').innerHTML = 'Please upload file having extensions .jpeg/.jpg/.png only.';
            document.getElementById('errormsgdiv').style.display = '';
            fileInput.value = '';


        }else{
            reader.onload = function () {
                const output = document.getElementById('output_image');
                output.src = reader.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    }

   
}
