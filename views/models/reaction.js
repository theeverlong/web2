const mongoose = require('mongoose');
require('dotenv').config();

const dbUrl = process.env.MONGO_URL;

const connectOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true
};

let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

mongoose.connect(dbUrl, connectOptions)
    .then(() => console.log('Mongo database connected'))
    .catch(() => console.log('ERROR: Mongo database not connected'));

const ReactionSchema = new Schema({
    message: { type: String, default: 'default' },
    time: { type: Date, default: Date.now },
    authorID: {type: ObjectId, ref: 'User'},
    receiverID: {type: ObjectId, ref: 'User'},
    storyID: {type: ObjectId, ref: 'Story'},
    versionKey: false
});

ReactionSchema.virtual('id').get(function(){
    return this._id;
});

const ReactionModel = mongoose.model('Reaction', ReactionSchema);

class Reaction {
    constructor(id, message, time, authorID, receiverID, storyID) {
        this.id = id;
        this.message = message;
        this.time = time;
        this.authorID = authorID;
        this.receiverID = receiverID;
        this.storyID = storyID;
    }

    static insert(reaction){
        return new ReactionModel(reaction).save();
    }

    static getAllByAuthorID(id) {
        return ReactionModel.find({authorID : id});
    }

    static getAllByReceiverID(id){
        return ReactionModel.find({receiverID : id});
    }

    static getAllByStoryID(id){
        return ReactionModel.find({storyID : id});
    }

    static deleteById(id) {
        return ReactionModel.deleteOne({_id : id});
    }

    static getById(id) {
        return ReactionModel.findById(id);
    }
}

module.exports = Reaction;