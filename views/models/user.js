const mongoose = require('mongoose');
require('dotenv').config();

const dbUrl = process.env.MONGO_URL;

const connectOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
};

let Schema = mongoose.Schema;

mongoose.set('useCreateIndex', true);

mongoose.connect(dbUrl, connectOptions)
    .then(() => console.log('Mongo database connected'))
    .catch(() => console.log('ERROR: Mongo database not connected'));

const UserSchema = new Schema({
    login: { type: String },
    passwordHash: { type: String },
    role: { type: Number },
    fullname: { type: String },
    registeredAt: { type: Date },
    bio: { type: String},
    avaUrl: { type: String },
    telegramUsername: { type: String },
    telegramChatID: {type: Number},
    facebookID: {type: Number},
    versionKey: false
});

UserSchema.virtual('id').get(function () {
    return this._id;
});

const UserModel = mongoose.model('User', UserSchema);

class User {
    constructor(id, login, passwordHash, role, fullname, registeredAt, bio, avaUrl, telegramUsername, telegramChatID, facebookID) {
        this.id = id;
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
        this.fullname = fullname;
        this.bio = bio;
        this.registeredAt = registeredAt;
        this.avaUrl = avaUrl;
        this.telegramUsername = telegramUsername;
        this.telegramChatID = telegramChatID;
        this.facebookID = facebookID;
    }

    static getAll() {
        return UserModel.find();
    }

    static getById(id) {
        return UserModel.findById(id);
    }

    static getByFacebookID(id){
        return UserModel.findOne({ facebookID: id });
    }

    static getByUsername(username) {
        return UserModel.findOne({ login: username});
    }

    static getByTelegramUsername(username) {
        return UserModel.findOne({ telegramUsername: username});
    }

    static insert(user) {
        return new UserModel(user).save();
    }

    static checkLoginUniqueness(newLogin){
        return UserModel.findOne({login: newLogin});
    }

    static update(user) {
        return UserModel.updateOne({ _id: user.id }, user);
    }

    static findAndUpdate(user){
        return UserModel.findOneAndUpdate({ _id: user.id }, user,  {new: true, useFindAndModify: false});
    }   

    static deleteById(id) {
        return UserModel.deleteOne({ _id: id });
    }

    static findAndDeleteById(id){
        return UserModel.findOneAndDelete({ _id: id });
    }

    static getByLoginAndPasswordHash(login, passHash){
        return UserModel.findOne({login: login, passwordHash: passHash});
    }

}

module.exports = User;
