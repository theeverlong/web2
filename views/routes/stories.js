const express = require('express');
const router = express.Router();
const Story = require('../models/story');
const Datauri = require('datauri');
const moment = require('moment');
const User = require('../models/user');
const Highlight = require('../models/highlight');
const asyncHandler = require('express-async-handler')
const mongoose = require('mongoose');

require('dotenv').config();

const multer = require('multer');
const path = require('path');

const storage = multer.memoryStorage();

let upload = multer({
    storage: storage,
    limits: { fileSize: 1000000 * 5 },
    fileFilter: function (req, file, cb) {
        if (file.mimetype !== 'image/png' && file.mimetype !== 'image/jpeg') {
            req.fileValidationError = true;
            return cb(new Error('Incorrect format'));
        }
        return cb(null, true);
    }
});

const dUri = new Datauri();
const dataUri = req => dUri.format(path.extname(req.file.originalname).toString(), req.file.buffer);
const cloudinary = require('cloudinary');

cloudinary.config({
    cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET
});

router.get('/new', (req, res) => {
    if (!req.user) return res.redirect('/auth/login');
    res.render('newStory', {
        isSignedIn: (typeof req.user === 'undefined') ? 'display: none' : '',
        isSignedOut: (typeof req.user !== 'undefined') ? 'display: none' : '',
        user: req.user
    });
});

router.get('/:id', asyncHandler(async (req, res) => {
    if (!req.user) return res.redirect('/auth/login');
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.redirect('/404');
    try {
        const story = await Story.getById(req.params.id);
        if (!story) return res.redirect('/404');
        const author = await User.getById(story.authorId);
        if (!author) return res.redirect('/404');

        const now = moment(new Date()); 
        const end = moment(story.expiresAt); 
        const time = end - now;
        let timeLeft;
        if(time > 0) timeLeft = 'story expires ' + moment(story.expiresAt).fromNow();
        else timeLeft = 'story expired ' +  moment(story.expiresAt).fromNow();

        if (req.user.id.toString() !== story.authorId.toString()) {
            if (story.viewsIDs) {
                if (!story.viewsIDs.includes(req.user.id)) {
                    story.viewsIDs.push(req.user.id);
                    await Story.update(story);
                }
            }

        }

        let views = [];
        if (req.user.id.toString() === story.authorId.toString()) {
            if (story.viewsIDs) {
                for (let i = 0; i < story.viewsIDs.length; i++) {
                    let user = await User.getById(story.viewsIDs[i]);
                    views.push(user);
                }
            }

        }

        res.render('story', {
            renderedUser: author,
            story: story,
            isReactionVisible: (req.user.id.toString() === author.id.toString()) ? 'display: none' : '',
            isSignedIn: (typeof req.user === 'undefined') ? 'display: none' : '',
            isSignedOut: (typeof req.user !== 'undefined') ? 'display: none' : '',
            isEditing: (req.user.id.toString() === author.id.toString() || req.user.role === 1) ? '' : 'display: none',
            views: views,
            timeLeft: timeLeft,
            user: req.user
        });


    }
    catch (err) {
        console.log(err);
        return res.status(500).send({ error: 'Internal Server Error' })
    }
}));

router.post('/story', (req, res) => {
    if (!req.user) return res.redirect('/auth/login');
    upload.single('storyImage')(req, res, err => {
        if (err) {
            res.status(400).send({ error: 'Bad Request' });
        }
        else {
            if (req.file) {
                if (req.fileValidationError) {
                    return res.status(400).send({ error: 'Invalid image file' });
                }
                const file = dataUri(req).content;
                return cloudinary.uploader.upload(file)
                    .then((result) => {
                        const image = result.url;
                        if (typeof req.body.title === 'undefined') throw '400';
                        const title = req.body.title;
                        if (title.length < 1 || title.length > 50) throw '400';
                        const desc = (typeof req.body.desc === 'undefined') ? '' : req.body.desc;
                        if (typeof req.body.expireTime === 'undefined') throw '400';
                        const expireTime = req.body.expireTime;
                        if (expireTime < 1 || expireTime > 72) throw '400';
                        const currentTime = moment().format();
                        const expiresAt = moment().add(expireTime, 'h');
                        return Story.insert(new Story(null, title, currentTime, expiresAt, desc, true, image, req.user.id, null));
                    })
                    .then(newStory => res.redirect('/stories/' + newStory.id))
                    .catch(err => {
                        if (err === '400') res.status(400).send({ error: 'Bad Request' });
                        if (err.http_code === 400) res.status(400).send({ error: 'Bad Request' });
                        res.status(500).send({ error: 'Internal Server Error' });
                    });
            }
            else {
                res.status(400).send({ error: 'No file uploaded' });
            }
        }
    });
});

router.post('/delete/:id', asyncHandler(async (req, res) => {
    if (!req.user) return res.redirect('/auth/login');
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.redirect('/404');
    try {
        const story = await Story.getById(req.params.id);
        if (req.user.id.toString() === story.authorId.toString() || req.user.role === 1) {
            image = story.imageUrl;
            await Story.deleteById(req.params.id);
            await deleteImageCloudinary(image);
            await Highlight.removeStoryFromAll(req.params.id);
            return res.redirect('/users/' + req.user.id);
        }

        else {
            return res.status(404).send({ 'error': 'Forbidden' });
        }
    }
    catch (error) {
        console.log(error);
        res.status(500).send({ error: 'Internal Server Error' });
    }

}));

function deleteImageCloudinary(image) {
    return new Promise((resolve, reject) => {
        cloudinary.v2.api.delete_resources(image,
            (error, result) => {
                if (error) reject(error);
                else resolve(result);
            });
    });
}



module.exports = router;
