const express = require('express');
const passport = require('passport');
const mongoose = require('mongoose');
const Datauri = require('datauri');
const asyncHandler = require('express-async-handler');

const saltedMd5 = require('salted-md5');
const serverSalt = 'SUPER-S@LT!';

const router = express.Router();

const bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
router.use(bodyParser.json({ type: 'application/vnd.api+json' }));

const User = require('../models/user');
const Story = require('../models/story');
const Highlight = require('../models/highlight');
const Reaction = require('../models/reaction');

const multer = require('multer');
const path = require('path');
const storage = multer.memoryStorage();

let upload = multer({
    storage: storage,
    limits: { fileSize: 1000000 * 5 },
    fileFilter: function (req, file, cb) {
        let filetypes = /jpeg|png|jpg/;
        let mimetype = filetypes.test(file.mimetype);
        let extname = filetypes.test(path.extname(file.originalname).toLowerCase());
        if (mimetype && extname) {
            return cb(null, true);
        }
        cb("Error: File upload only supports the following filetypes - " + filetypes);
    }
});

const dUri = new Datauri();
const dataUri = req => dUri.format(path.extname(req.file.originalname).toString(), req.file.buffer);
const cloudinary = require('cloudinary');

cloudinary.config({
    cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET
});

router.get('/', (req, res) => {
    res.json();
});

router.get('/me',
    passport.authenticate('basic', { session: false }),
    function (req, res) {
        res.status(200).json(req.user);
    });

router.get('/users1', passport.authenticate('basic', { session: false }), (req, res) => {
    checkAdmin(req, res);
    const pageSize = 2;
    const page = (!req.query.page) ? 1 : req.query.page;
    const search = (!req.query.search) ? null : req.query.search.toLowerCase();
    User.getAll()
        .then(users => {
            if (search !== null) {
                for (let i = 0; i < users.length; i++) {
                    if (!users[i].login.toLowerCase().includes(search)) {
                        users.splice(i, 1);
                        i--;
                    }
                }
            }
            const pagesTotal = Math.ceil(users.length / pageSize);
            if (users.length === 0 && search !== null) return res.status(404).send({ error: 'Not Found' });
            if (page > pagesTotal || page < 1) return res.status(400).send({ error: 'Bad Request' });
            let index = (page - 1) * pageSize;
            return res.status(200).json(users.slice((page - 1) * pageSize, index + pageSize));
        })
        .catch(() => res.status(500).send({ error: 'Internal Server Error' }));
});

router.get('/users1/:id', passport.authenticate('basic', { session: false }), (req, res) => {
    checkAdmin(req, res);
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(400).send({ error: 'Bad Request' });
    User.getById(req.params.id)
        .then(user => {
            if (user) {
                res.status(200).json(user);
            }
            else {
                return res.status(404).send({ error: "Not Found" });
            }
        })
        .catch(() => res.status(500).send({ error: 'Internal Server Error' }));
});

router.post('/users', passport.authenticate('basic', { session: false }), (req, res) => {
    checkAdmin(req, res);
    upload.single('avaImage')(req, res, err => {
        if (err) {
            res.status(400).send({ error: 'Bad Request' });
        }
        else {
            const login = (typeof req.body.login === 'undefined') ? '' : req.body.login;
            if (login.length < 3 || login.length > 20) return res.status(400).send({ error: 'Bad Request' });
            const pass = (typeof req.body.pass === 'undefined') ? '' : req.body.pass;
            if (pass.length < 6 || pass.length > 256) return res.status(400).send({ error: 'Bad Request' });
            const passHash = saltedMd5(pass, serverSalt);
            const fullname = (typeof req.body.fullname === 'undefined') ? '' : req.body.fullname;
            if (fullname.length > 256) return res.status(400).send({ error: 'Bad Request' });
            const role = (typeof req.body.role === 'undefined') ? 0 : req.body.role;
            if (role !== 1 && role !== 0) return res.status(400).send({ error: 'Bad Request' });
            User.checkLoginUniqueness(login)
                .then(user => {
                    if (user) throw '400';
                    else {
                        if (req.file) {
                            const file = dataUri(req).content;
                            return cloudinary.uploader.upload(file);
                        } else return Promise.resolve({ url: 'https://res.cloudinary.com/dw5e6ptdy/image/upload/v1573832746/user_c4sbyn.png' });
                    }
                })
                .then(upload => {
                    const image = upload.url;
                    return User.insert(new User(null, login, passHash, role, fullname, Date.now(), image));
                })
                .then(newUser => res.status(201).json(newUser))
                .catch(error => {
                    if (error === '400') res.status(400).send({ error: 'Bad Request' });
                    res.status(500).send({ error: 'Internal Server Error' });
                });
        }
    });
});

router.delete('/users/:id', passport.authenticate('basic', { session: false }), (req, res) => {
    checkAdmin(req, res);
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(400).send({ error: 'Bad Request' });
    let deletedUser;
    User.findAndDeleteById(req.params.id)
        .then((user) => {
            if (!user) throw '404';
            else {
                deletedUser = user;
                let beginIndex = user.imageUrl.lastIndexOf('/');
                let endIndex = user.imageUrl.lastIndexOf('.');
                const image = user.imageUrl.substring(beginIndex + 1, endIndex);
                if (image !== 'user_c4sbyn') {
                    return deleteImageCloudinary(image);
                }
            }
        })
        .then(() => {
            return res.status(200).json(deletedUser);
        }
        )
        .catch((error) => {
            if (error === '404') res.status(404).send({ error: "Not Found" });
            res.status(500).send({ error: 'Internal Server Error' });
        });
});

router.put('/users/:id', passport.authenticate('basic', { session: false }), (req, res) => {
    checkAdmin(req, res);
    upload.single('avaImage')(req, res, err => {
        if (err) {
            res.status(400).send({ error: 'Bad Request' });
        }
        else {
            if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(400).send({ error: 'Bad Request' });
            let userToUpdate;
            User.getById(req.params.id)
                .then(user => {
                    if (user) {
                        if (typeof req.body.pass !== 'undefined') {
                            const pass = req.body.pass;
                            if (pass.length < 6 || pass.length > 256) throw '400';
                            else user.passwordHash = saltedMd5(pass, serverSalt);
                        }
                        if (typeof req.body.fullname !== 'undefined') user.fullname = req.body.fullname;
                        if (user.fullname.length > 256) throw '400';
                        if (typeof req.body.role !== 'undefined') {
                            if (req.body.role !== 1 && req.body.role !== 0) throw '400';
                            else user.role = req.body.role;
                        }
                        userToUpdate = user;
                        if (req.file) {
                            const file = dataUri(req).content;
                            return cloudinary.uploader.upload(file);
                        }
                        else {
                            return Promise.resolve(null);
                        }
                    }
                    else throw '400';
                })
                .then(upload => {
                    if (upload) userToUpdate.avaUrl = upload.url;
                    return User.findAndUpdate(userToUpdate);
                })
                .then(updatedUser => {
                    res.status(200).json(updatedUser);
                })
                .catch(error => {
                    if (error === '400') res.status(400).send({ error: 'Bad Request' });
                    res.status(500).send({ error: 'Internal Server Error' });
                });
        }
    });

});

router.get('/stories1', passport.authenticate('basic', { session: false }), (req, res) => {
    checkAuth(req, res);
    const pageSize = 2;
    const page = (!req.query.page) ? 1 : req.query.page;
    const search = (!req.query.search) ? null : req.query.search.toLowerCase();
    Story.getAll()
        .then(stories => {
            if (search !== null) {
                for (let i = 0; i < stories.length; i++) {
                    if (!stories[i].title.toLowerCase().includes(search)) {
                        stories.splice(i, 1);
                        i--;
                    }
                }
            }
            const pagesTotal = Math.ceil(stories.length / pageSize);
            if (stories.length === 0 && search !== null) return res.status(404).send({ error: 'Not Found' });
            if (page > pagesTotal || page < 1) return res.status(400).send({ error: 'Bad Request' });
            let index = (page - 1) * pageSize;
            return res.status(200).json(stories.slice((page - 1) * pageSize, index + pageSize));
        })
        .catch(() => res.status(500).send({ error: 'Internal Server Error' }));
});

router.get('/stories1/:id', passport.authenticate('basic', { session: false }), (req, res) => {
    checkAuth(req, res);
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(400).send({ error: 'Incorrect ID type' });
    Story.getById(req.params.id)
        .then(story => {
            if (story) {
                res.status(200).json(story);
            }
            else {
                return res.status(404).send({ error: "Not Found" });
            }
        })
        .catch(() => res.status(500).send({ error: 'Internal Server Error' }));
});

router.post('/stories', passport.authenticate('basic', { session: false }), (req, res) => {
    checkAuth(req, res);
    upload.single('storyImage')(req, res, err => {
        if (err) {
            res.status(400).send({ error: 'Bad Request' });
        }
        else {
            const title = (typeof req.body.title === 'undefined') ? '' : req.body.title;
            if (title.length < 1 || title.length > 20) return res.status(400).send({ error: 'Bad Request' });
            const publishedAt = Date.now();
            const description = (typeof req.body.description === 'undefined') ? '' : req.body.description;
            if (description.length > 10000) return res.status(400).send({ error: 'Bad Request' });
            const isPublic = (typeof req.body.isPublic === 'undefined') ? false : req.body.isPublic;
            if (isPublic !== true && isPublic !== false) return res.status(400).send({ error: 'Bad Request' });
            const authorID = req.user.id;
            if (req.file) {
                const file = dataUri(req).content;
                cloudinary.uploader.upload(file)
                    .then(upload => {
                        const image = upload.url;
                        return Story.insert(new Story(null, title, publishedAt, null, description, isPublic, image, authorID));
                    })
                    .then(newUser => res.status(201).json(newUser))
                    .catch(() => {
                        res.status(500).send({ error: 'Internal Server Error' });
                    });
            } else {
                const image = 'https://res.cloudinary.com/dw5e6ptdy/image/upload/v1572560587/sample.jpg';
                Story.insert(new Story(null, title, publishedAt, null, description, isPublic, image, authorID))
                    .then(newUser => res.status(201).json(newUser))
                    .catch(() => {
                        res.status(500).send({ error: 'Internal Server Error' });
                    });
            }
        }
    });
});

router.delete('/stories/:id', passport.authenticate('basic', { session: false }), (req, res) => {
    checkAuth(req, res);
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(400).send({ error: 'Bad Request' });
    let deletedStory;
    Story.findAndDeleteById(req.params.id)
        .then((story) => {
            if (!story) throw '404';
            else {
                deletedStory = story;
                let beginIndex = story.imageUrl.lastIndexOf('/');
                let endIndex = story.imageUrl.lastIndexOf('.');
                const image = story.imageUrl.substring(beginIndex + 1, endIndex);
                if (image !== 'sample') {
                    return deleteImageCloudinary(image);
                }
            }
        })
        .then(() => {
            return res.status(200).json(deletedStory);
        }
        )
        .catch((error) => {
            if (error === '404') res.status(404).send({ error: "Not Found" });
            res.status(500).send({ error: 'Internal Server Error' });
        });
});

router.put('/stories/:id', passport.authenticate('basic', { session: false }), (req, res) => {
    checkAuth(req, res);
    upload.single('storyImage')(req, res, err => {
        if (err) {
            res.status(400).send({ error: 'Bad Request' });
        }
        else {
            if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(400).send({ error: 'Bad Request' });
            let storyToUpdate;
            Story.getById(req.params.id)
                .then(story => {
                    if (story) {
                        if (typeof req.body.title !== 'undefined') story.title = req.body.title;
                        if (story.title.length > 20) throw '400';
                        if (typeof req.body.description !== 'undefined') story.description = req.body.description;
                        if (story.description.length > 10000) throw '400';
                        if (typeof req.body.isPublic !== 'undefined') story.isPublic = req.body.isPublic;
                        if (story.isPublic !== true && story.isPublic !== false) throw '400';
                        storyToUpdate = story;
                        if (req.file) {
                            const file = dataUri(req).content;
                            return cloudinary.uploader.upload(file);
                        }
                        else {
                            return Promise.resolve(null);
                        }
                    }
                    else throw '400';
                })
                .then(upload => {
                    if (upload) storyToUpdate.avaUrl = upload.url;
                    return Story.findAndUpdate(storyToUpdate);
                })
                .then(updatedStory => {
                    res.status(200).json(updatedStory);
                })
                .catch(error => {
                    if (error === '400') res.status(400).send({ error: 'Bad Request' });
                    res.status(500).send({ error: 'Internal Server Error' });
                });
        }
    });

});

router.get('/highlights', passport.authenticate('basic', { session: false }), (req, res) => {
    checkAuth(req, res);
    const pageSize = 2;
    const page = (!req.query.page) ? 1 : req.query.page;
    const search = (!req.query.search) ? null : req.query.search.toLowerCase();
    Highlight.getAll()
        .then(highlights => {
            if (search !== null) {
                for (let i = 0; i < highlights.length; i++) {
                    if (!highlights[i].title.toLowerCase().includes(search)) {
                        highlights.splice(i, 1);
                        i--;
                    }
                }
            }
            const pagesTotal = Math.ceil(highlights.length / pageSize);
            if (highlights.length === 0 && search !== null) return res.status(404).send({ error: 'Not Found' });
            if (page > pagesTotal || page < 1) return res.status(400).send({ error: 'Bad Request' });
            let index = (page - 1) * pageSize;
            return res.status(200).json(highlights.slice((page - 1) * pageSize, index + pageSize));
        })
        .catch(() => res.status(500).send({ error: 'Internal Server Error' }));
});

router.get('/highlights/:id', passport.authenticate('basic', { session: false }), (req, res) => {
    checkAuth(req, res);
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(400).send({ error: 'Incorrect ID type' });
    Highlight.getById(req.params.id)
        .then(story => {
            if (story) {
                res.status(200).json(story);
            }
            else {
                return res.status(404).send({ error: "Not Found" });
            }
        })
        .catch(() => res.status(500).send({ error: 'Internal Server Error' }));
});

router.post('/highlights', passport.authenticate('basic', { session: false }), (req, res) => {
    checkAuth(req, res);
    upload.single('highlightImage')(req, res, err => {
        if (err) {
            res.status(400).send({ error: 'Bad Request' });
        }
        else {
            const title = (typeof req.body.title === 'undefined') ? '' : req.body.title;
            if (title.length < 1 || title.length > 20) return res.status(400).send({ error: 'Bad Request' });
            const description = (typeof req.body.description === 'undefined') ? '' : req.body.description;
            if (description.length > 10000) return res.status(400).send({ error: 'Bad Request' });
            const isPublic = (typeof req.body.role === 'undefined') ? false : req.body.isPublic;
            if (isPublic !== true && isPublic !== false) return res.status(400).send({ error: 'Bad Request' });
            let storiesIDs = (typeof req.body.title === 'undefined') ? '' : req.body.storiesIDs;
            const authorID = req.user.id;

            if (req.file) {
                const file = dataUri(req).content;
                cloudinary.uploader.upload(file)
                    .then(upload => {
                        const image = upload.url;
                        return Highlight.insert(new Highlight(null, title, image, description, isPublic, storiesIDs, authorID));
                    })
                    .then(newHighlight => res.status(201).json(newHighlight))
                    .catch(() => {
                        res.status(500).send({ error: 'Internal Server Error' });
                    });
            } else {
                const image = 'https://res.cloudinary.com/dw5e6ptdy/image/upload/v1572560587/sample.jpg';
                Highlight.insert(new Highlight(null, title, image, description, isPublic, storiesIDs, authorID))
                    .then(newHighlight => res.status(201).json(newHighlight))
                    .catch(() => {
                        res.status(500).send({ error: 'Internal Server Error' });
                    });
            }
        }
    });
});

router.delete('/highlights/:id', passport.authenticate('basic', { session: false }), (req, res) => {
    checkAuth(req, res);
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(400).send({ error: 'Bad Request' });
    let deletedHighlight;
    Highlight.findAndDeleteById(req.params.id)
        .then((highlight) => {
            if (!highlight) throw '404';
            else {
                deletedHighlight = highlight;
                let beginIndex = highlight.imageUrl.lastIndexOf('/');
                let endIndex = highlight.imageUrl.lastIndexOf('.');
                const image = highlight.imageUrl.substring(beginIndex + 1, endIndex);
                if (image !== 'sample') {
                    return deleteImageCloudinary(image);
                }
            }
        })
        .then(() => {
            return res.status(200).json(deletedHighlight);
        }
        )
        .catch((error) => {
            if (error === '404') res.status(404).send({ error: "Not Found" });
            res.status(500).send({ error: 'Internal Server Error' });
        });
});

router.put('/highlights/:id', passport.authenticate('basic', { session: false }), (req, res) => {
    checkAuth(req, res);
    upload.single('highlightImage')(req, res, err => {
        if (err) {
            res.status(400).send({ error: 'Bad Request' });
        }
        else {
            if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(400).send({ error: 'Bad Request' });
            let highlightToUpdate;
            Highlight.getById(req.params.id)
                .then(highlight => {
                    if (highlight) {
                        if (typeof req.body.title !== 'undefined') highlight.title = req.body.title;
                        if (highlight.title.length > 20) throw '400';
                        if (typeof req.body.description !== 'undefined') highlight.description = req.body.description;
                        if (highlight.description.length > 10000) throw '400';
                        if (typeof req.body.isPublic !== 'undefined') highlight.isPublic = req.body.isPublic;
                        if (highlight.isPublic !== true && highlight.isPublic !== false) throw '400';
                        highlight.storiesIDs = [];
                        if (typeof req.body.storiesIDs !== 'undefined') highlight.storiesIDs = req.body.storiesIDs;
                        highlightToUpdate = highlight;
                        if (req.file) {
                            const file = dataUri(req).content;
                            return cloudinary.uploader.upload(file);
                        }
                        else {
                            return Promise.resolve(null);
                        }
                    }
                    else throw '400';
                })
                .then(upload => {
                    if (upload) highlightToUpdate.avaUrl = upload.url;
                    return Highlight.findAndUpdate(highlightToUpdate);
                })
                .then(updatedHighlight => {
                    res.status(200).json(updatedHighlight);
                })
                .catch(error => {
                    if (error === '400') res.status(400).send({ error: 'Bad Request' });
                    res.status(500).send({ error: 'Internal Server Error' });
                });
        }
    });

});

//done
router.get('/userHighlights/:id', asyncHandler(async (req, res) => {
    if(!req.user) return res.status(403).send({ error: 'Unauthorized'});   
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(400).send({ error: 'Bad Request' });
    try{
        const user = await User.getById(req.params.id);
        if (!user) {
            return res.status(404).send({ error: 'Not Found' });
        }
        const highlights = await Highlight.getAllUserHighlightsById(req.params.id);

        const pageSize = 3;
        const page = (!req.query.page) ? 1 : req.query.page;
        const search = (!req.query.search) ? null : req.query.search.toLowerCase();

        if (search !== null) {
            for (let i = 0; i < highlights.length; i++) {
                if (!highlights[i].title.toLowerCase().includes(search)) {
                    highlights.splice(i, 1);
                    i--;
                }
            }
        }
        const pagesTotal = Math.ceil(highlights.length / pageSize);
        if(req.query.page){
            if (req.query.page > pagesTotal || req.query.page < 1) return res.status(400).send({ error: 'Bad Request' });

        }
        let index = (page - 1) * pageSize;
        const length = highlights.length;
        return res.status(200).json({
            'highlights': highlights.slice((page - 1) * pageSize, index + pageSize),
            'highlightsLength': length
        });
    }
    catch(error){
        return res.status(500).send({ error: 'Internal Server Error' });
    }
}))

router.get('/validUsername/:login', (req, res) => {
    User.checkLoginUniqueness(req.params.login)
        .then((user) => {
            if (!user) {
                res.status(200).send({ message: 'Valid Username' });
            }
            else {
                res.status(409).send({
                    error: 'Non Unique Username',
                    username: user.login
                });
            }
        })
        .catch(err => {
            res.status(500).send({ error: 'Internal Server Error' });
        });
});

router.get('/publicUserStories/:id', asyncHandler(async (req, res) => {
    if(!req.user) res.status(403).send({ error: 'Unauthorized'});
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(400).send({ error: 'Bad Request' });
    try {
        const user = await User.getById(req.params.id);
        if (!user) {
            return res.status(404).send({ error: 'Not Found' });
        }
        const stories = await Story.getAllUserPublicStoriesById(req.params.id);
        const pageSize = 3;
        const page = (!req.query.page) ? 1 : req.query.page;
        const search = (!req.query.search) ? null : req.query.search.toLowerCase();
        if (search !== null) {
            for (let i = 0; i < stories.length; i++) {
                if (!stories[i].title.toLowerCase().includes(search)) {
                    stories.splice(i, 1);
                    i--;
                }
            }
        }
        const pagesTotal = Math.ceil(stories.length / pageSize);
        if(req.query.page){
            if (req.query.page > pagesTotal || req.query.page < 1) return res.status(400).send({ error: 'Bad Request' });

        }
        let index = (page - 1) * pageSize;
        const length = stories.length;
        return res.status(200).json({
            'stories': stories.slice((page - 1) * pageSize, index + pageSize),
            'storiesLength': length
        });
    } catch (err) {
        return res.status(500).send({ error: 'Internal Server Error' });
    }
}));

router.get('/users' ,asyncHandler(async (req, res) => {
    if(!req.user) res.status(403).send({ error: 'Unauthorized'});
    try{
        const users = await User.getAll();
        const pageSize = 5;
        const page = (!req.query.page) ? 1 : req.query.page;
        const search = (!req.query.search) ? null : req.query.search.toLowerCase();
        if (search !== null) {
            for (let i = 0; i < users.length; i++) {
                if (!users[i].login.toLowerCase().includes(search)) {
                    users.splice(i, 1);
                    i--;
                }
            }
        }
        const length = users.length;
        const pagesTotal = Math.ceil(users.length / pageSize);
        if(req.query.page)
        {
            if (req.query.page > pagesTotal || req.query.page < 1) return res.status(400).send({ error: 'Bad Request' });
        }
        let index = (page - 1) * pageSize;
        return res.status(200).json({
            'users': users.slice((page - 1) * pageSize, index + pageSize),
            'usersLength': length
        });
    }
    catch (error) {
        console.log(error);
        return res.status(500).send({ error: 'Internal Server Error'});
    }
}));

router.get('/sentReactions', asyncHandler(async (req, res) => {
    if(!req.user) res.status(403).send({ error: 'Unauthorized'});
    try{
        const sentReactions = await Reaction.getAllByAuthorID(req.user.id);
        const length = sentReactions.length;
        const pageSize = 5;
        const page = (!req.query.page) ? 1 : req.query.page;
        const pagesTotal = Math.ceil(sentReactions.length / pageSize);
        if (sentReactions.length === 0) return res.status(200).send({ 'sentReactions': [],  'sentReactionsLength': 0});
        if (page > pagesTotal || page < 1) return res.status(400).send({ error: 'Bad Request' });
        let index = (page - 1) * pageSize;
        return res.status(200).json({
            'sentReactions': sentReactions.slice((page - 1) * pageSize, index + pageSize),
            'sentReactionsLength': length
        })
    }catch (error) {
        console.log(error);
        res.status(500).send({ error: 'Internal Server Error' });
    }
}));

router.get('/receivedReactions', asyncHandler(async (req, res) => {
    if(!req.user) res.status(403).send({ error: 'Unauthorized'});
    try{
        const sentReactions = await Reaction.getAllByReceiverID(req.user.id);
        const length = sentReactions.length;
        const pageSize = 5;
        const page = (!req.query.page) ? 1 : req.query.page;
        const pagesTotal = Math.ceil(sentReactions.length / pageSize);
        if (sentReactions.length === 0) return res.status(200).send({ 'receivedReactions': [],  'receivedReactionsLength': 0});
        if (page > pagesTotal || page < 1) return res.status(400).send({ error: 'Bad Request' });
        let index = (page - 1) * pageSize;

        return res.status(200).json({
            'receivedReactions': sentReactions.slice((page - 1) * pageSize, index + pageSize),
            'receivedReactionsLength': length
        })
    }catch (error) {
        console.log(error);
        res.status(500).send({ error: 'Internal Server Error' });
    }
}));

router.get('/users/:id', asyncHandler(async (req, res)=>{
    if(!req.user) res.status(403).send({ error: 'Unauthorized'});
    try{
        const user = await User.getById(req.params.id);
        if(!user) return res.status('404').send({ error: 'Not Found'});
        return res.status(200).json({ 'user' : user });
    }catch(error){
        console.log(error);
        return res.status(500).send({ error: 'Internal Server Error' });
    }
}));

router.get('/stories/:id', asyncHandler(async (req, res)=>{
    if(!req.user) res.status(403).send({ error: 'Unauthorized'});
    try{
        const story = await Story.getById(req.params.id);
        if(!story) return res.status('404').send({ error: 'Not Found'});
        return res.status(200).json({ 'story' : story });
    }catch(error){
        console.log(error);
        return res.status(500).send({ error: 'Internal Server Error' });
    }
}));

function checkAuth(req, res) {
    if (!req.user) return res.Status(401).send({ error: "Unauthorized" });
};

function checkAdmin(req, res) {
    if (!req.user) return res.Status(401).send({ error: "Unauthorized" });
    if (req.user.role !== 1) return res.Status(403).send({ error: "Forbidden" });
};

function deleteImageCloudinary(image) {
    return new Promise((resolve, reject) => {
        cloudinary.v2.api.delete_resources(image,
            (error, result) => {
                if (error) reject(error);
                else resolve(result);
            });
    });
}

module.exports = router;